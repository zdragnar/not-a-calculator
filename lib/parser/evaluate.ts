import { Operators } from "../types/operators";
import { BinaryOperatorNode, ExpressionNode, NodeType, SubExprNode, ValueNode } from "./types";

export const evaluate = (tree: ExpressionNode): number => {
  switch(tree.type) {
    case NodeType.Value: return evaluateValue(tree);
    case NodeType.Operator: return evaluateBinary(tree);
    case NodeType.SubExpr: return evaluateExpr(tree);
  }
}

const evaluateValue = (valueNode: ValueNode): number => Number(valueNode.value);

const evaluateBinary = (binaryNode: BinaryOperatorNode) => {
  const lhs = evaluate(binaryNode.left);
  const rhs = evaluate(binaryNode.right);

  switch(binaryNode.operator) {
    case Operators.Add: return lhs + rhs;
    case Operators.Subtract: return lhs - rhs;
    case Operators.Multiply: return lhs * rhs;
    case Operators.Divide: return lhs / rhs;
  }
}

const evaluateExpr = (exprNode: SubExprNode) => evaluate(exprNode.expr);
// shamelessly stolen from https://jorendorff.github.io/calc/docs/calculator-parser.html

export const tokenize = (code: string) => {
	const results = [];
	const tokenRegExp = /\s*([A-Za-z]+|[0-9]+|\S)\s*/g;

	let m;
	while ((m = tokenRegExp.exec(code)) !== null)
		results.push(m[1]);

	return results;
}
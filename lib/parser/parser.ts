import { Operators } from "../types/operators";
import { isNumericString } from "../types/predicates/isNumericString";
import { tokenize } from "./tokenizer"
import { ExpressionNode, NodeType } from "./types";



export const parse = (input: string): ExpressionNode => {
  const tokens = tokenize(input);

  let position = 0;

  const peek = () => tokens[position];

  const consume = () => position++;

  const parseValueExpr = (): ExpressionNode => {
    const t = peek();

    if (isNumericString(t)) {
      consume();
      return { type: NodeType.Value, value: t };
    } else if (t === '(') {
      const startPosition = position;
      consume();
      const expr = parseMultDivideExpr();
      if (peek() !== ')') {
        throw new SyntaxError(`Unbalanced parentheses; Closing paren for position ${startPosition} not found`);
      }
      consume();
      return { type: NodeType.SubExpr, expr };
    } else {
      throw new SyntaxError(`Position ${position} must be a number or expression`);
    }
  }

  const parseMultDivideExpr = (): ExpressionNode => {
    let expr: ExpressionNode = parseValueExpr();
    let t = peek();
    while (t === Operators.Multiply || t === Operators.Divide) {
      consume();
      const rhs = parseValueExpr();
      expr = { type: NodeType.Operator, operator: t as Operators, left: expr, right: rhs };
      t = peek();
    }

    return expr;
  }

  const parseAddSubExpr = () => {
    let expr: ExpressionNode = parseMultDivideExpr();
    let t = peek();
    while (t === Operators.Add || t === Operators.Subtract) {
      consume();
      const rhs = parseMultDivideExpr();
      expr = { type: NodeType.Operator, operator: t as Operators, left: expr, right: rhs };
      t = peek();
    }

    return expr;
  }

  const result = parseAddSubExpr();
  
  if (position !== tokens.length) throw new SyntaxError(`Not all tokens were parsed (starting at ${position})`);

  return result;
}
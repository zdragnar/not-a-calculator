import { Operators } from "../types/operators";

export enum NodeType {
  Operator = 'operator',
  Value = 'value',
  SubExpr = 'sub-expr'
}


export interface Node {
  type: NodeType;
}

export interface ValueNode extends Node {
  type: NodeType.Value;
  value: string;
}

export interface SubExprNode extends Node {
  type: NodeType.SubExpr;
  expr: ExpressionNode;
}

export interface BinaryOperatorNode extends Node {
  type: NodeType.Operator;
  operator: Operators;
  left: ExpressionNode;
  right: ExpressionNode;
}

export type ExpressionNode = ValueNode | BinaryOperatorNode | SubExprNode;

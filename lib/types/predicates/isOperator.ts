import { Operators } from "../operators"

export const isOperator = (v: string) => {
	return !!(v in Operators);
}
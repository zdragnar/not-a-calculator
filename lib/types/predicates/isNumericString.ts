export const isNumericString = (v: string) => {
  return !isNaN(Number(v));
}
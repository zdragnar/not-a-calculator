import { FC, MouseEvent, useCallback, useState } from "react";
import styled from 'lib/styled';
import { Button } from "components/button/button";
import { Operators } from "lib/types/operators";
import { evaluate } from "lib/parser/evaluate";
import { parse } from "lib/parser/parser";

const Column = styled.div`
  display: flex;
  flex-direction: column;
`;

const Row = styled.div`
  display: flex;
  flex-direction: row;
`;

const CharacterButton: FC<{ char: string; onPress: (v: string) => void}> = ({ char, onPress}) => {
  const handleClick = useCallback(e => {
    e.preventDefault();
    onPress(char);
  }, [char, onPress]);

  return (
    <Button onClick={handleClick}>{char}</Button>
  );
}

export const Calculator: FC = () => {
  const [input, setInput] = useState('');
  const [result, setResult] = useState<string | null>(null);
  const [error, setError] = useState<string | null>(null);

  const resetInput = () => setInput('');

  const onAddChar = useCallback((char: string) => {
    if (result !== '') setResult('');

    setInput(input + char);
  }, [input, result]);

  const onEvaluate = useCallback((e: MouseEvent) => {
    e.preventDefault();
    try {
      const newResult = evaluate(parse(input));
      setResult(`${newResult}`);
      setError(null);
    } catch(e) {
      if (e.message) setError(e.message);
      else setError('Generic error message here');
    }
  }, [input]); 

  return (
    <Column>
      {result && (
        <Row>Result: {result}</Row>
      )}
      {error && (
        <Row>Error: {error}</Row>
      )}
      <Row>
        <input readOnly value={input} />
      </Row>
      <Row>
        <CharacterButton char={Operators.Divide} onPress={onAddChar} />
        <CharacterButton char={Operators.Multiply} onPress={onAddChar} />
        <CharacterButton char={Operators.Subtract} onPress={onAddChar} />
        <CharacterButton char={Operators.Add} onPress={onAddChar} />
        <CharacterButton char={"("} onPress={onAddChar} />
        <CharacterButton char={")"} onPress={onAddChar} />
      </Row>
      <Row>
        <CharacterButton char={"7"} onPress={onAddChar} />
        <CharacterButton char={"8"} onPress={onAddChar} />
        <CharacterButton char={"9"} onPress={onAddChar} />
      </Row>
      <Row>
        <CharacterButton char={"4"} onPress={onAddChar} />
        <CharacterButton char={"5"} onPress={onAddChar} />
        <CharacterButton char={"6"} onPress={onAddChar} />
      </Row>
      <Row>
        <CharacterButton char={"1"} onPress={onAddChar} />
        <CharacterButton char={"2"} onPress={onAddChar} />
        <CharacterButton char={"3"} onPress={onAddChar} />
      </Row>

      <Row>
        <Button onClick={resetInput}>Clear</Button>
        <CharacterButton char={"0"} onPress={onAddChar} />
        <Button onClick={onEvaluate}>=</Button>
      </Row>
    </Column>
  )
}